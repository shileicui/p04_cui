//
//  AppDelegate.h
//  p04_cui
//
//  Created by SHILEI CUI on 3/13/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

